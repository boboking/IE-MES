package com.iemes.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.ContainerFormMap;
import com.iemes.entity.ContainerSFCFormMap;
import com.iemes.entity.ContainerTypeFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.service.PackingService;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
@Service
public class PackingServiceImpl implements PackingService {

	private Logger log = Logger.getLogger(this.getClass());
	
	@Inject
	private BaseMapper baseMapper;
	
	@Override
	@Transactional
	public void saveContainerSFC(ContainerTypeFormMap containerFormMap)
			throws BusinessException {
		try {
			String containerId =  containerFormMap.getStr("id");
			if(StringUtils.isEmpty(containerId)){
				throw new BusinessException("请先检索在保存！");
			}
			
			ContainerSFCFormMap containerSFCFormMap = new ContainerSFCFormMap();
			containerSFCFormMap.put("container_id", containerId);
			containerSFCFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			baseMapper.deleteByNames(containerSFCFormMap);
			
			String childSfc = containerFormMap.getStr("containerUdata");
			if (!StringUtils.isEmpty(childSfc)) {
				JSONArray jsonArray = JSONArray.fromObject(childSfc);
				for (int i = 0; i < jsonArray.size(); i++) {
					JSONObject objectRow = jsonArray.getJSONObject(i);
					JSONObject sfcCell = objectRow.getJSONObject("value");
					String sfc = sfcCell.getString("value");
					containerSFCFormMap.put("id", UUIDUtils.getUUID());
					containerSFCFormMap.put("sfc", sfc);
					containerSFCFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
					containerSFCFormMap.put("create_time", DateUtils.getStringDateTime());
					baseMapper.addEntity(containerSFCFormMap);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("包装信息保存失败！" + e.getMessage());
		}

	}

	/**
	 * 1、判断是否已经包装过了，是则不允许包装
	 * 2、判断是否已经装满，如果已包装数量+当前包装数等于总数量，则将容器自动关闭
	 * 
	 */
	@Override
	@Transactional
	public String packing(ContainerSFCFormMap containerSFCFormMap) throws BusinessException {
		try {
			String rs = "";
			String containerId = containerSFCFormMap.getStr("container_id");
			String containerTypeId = containerSFCFormMap.getStr("container_type_id");
			
			String max_num = containerSFCFormMap.getStr("max_num");
			String packingNum = containerSFCFormMap.getStr("packingNum");
			

			//判断是否为最后一个，是则关闭容器
			ContainerFormMap containerFormMap2 = new ContainerFormMap();
			containerFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			containerFormMap2.put("container_id", containerId);
			List<ContainerFormMap> list2 = baseMapper.findByNames(containerFormMap2);
			if (Integer.parseInt(max_num) - Integer.parseInt(packingNum) == 1) {
				if (ListUtils.isNotNull(list2)) {
					ContainerFormMap containerFormMap = list2.get(0);
					containerFormMap.put("container_status", -1);
					baseMapper.editEntity(containerFormMap);
					rs = "包装完毕，容器已自动关闭！！！";
				}else {
					ContainerFormMap containerFormMap = new ContainerFormMap();
					containerFormMap.put("id", UUIDUtils.getUUID());
					containerFormMap.put("container_id", containerId);
					containerFormMap.put("container_type_id", containerTypeId);
					containerFormMap.put("container_status", -1);
					containerFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
					baseMapper.addEntity(containerFormMap);
					rs = "包装完毕，容器已自动关闭！！！";
				}
			}else {
				if (!ListUtils.isNotNull(list2)) {
					ContainerFormMap containerFormMap = new ContainerFormMap();
					containerFormMap.put("id", UUIDUtils.getUUID());
					containerFormMap.put("container_id", containerId);
					containerFormMap.put("container_type_id", containerTypeId);
					containerFormMap.put("container_status", 1);
					containerFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
					baseMapper.addEntity(containerFormMap);
				}
			}
			
			containerSFCFormMap.put("id", UUIDUtils.getUUID());
			containerSFCFormMap.put("create_time", DateUtils.getStringDateTime());
			containerSFCFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
			baseMapper.addEntity(containerSFCFormMap);
			return rs;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("包装失败！" + e.getMessage());
		}
		
	}

	@Override
	@Transactional
	public void unPacking(ContainerSFCFormMap containerSFCFormMap) throws BusinessException {
		try {
			containerSFCFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			List<ContainerSFCFormMap> list = baseMapper.findByNames(containerSFCFormMap);
			ContainerSFCFormMap containerSFCFormMap2 = list.get(0);
			baseMapper.deleteByNames(containerSFCFormMap2);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("解包失败！" + e.getMessage());
		}
	}

	@Override
	@Transactional
	public void openContainer(ContainerFormMap containerFormMap) throws BusinessException {
		try {
			List<ContainerFormMap> list = baseMapper.findByNames(containerFormMap);
			if (!ListUtils.isNotNull(list)) {
				String msg = "容器不存在，无法执行解包动作！！！";
				log.error(msg);
				throw new BusinessException(msg);
			}
			ContainerFormMap containerFormMap2 = list.get(0);
			containerFormMap2.put("container_status", 1);
			baseMapper.editEntity(containerFormMap2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	@Transactional
	public void closeContainer(ContainerFormMap containerFormMap) throws BusinessException {
		try {
			List<ContainerFormMap> list = baseMapper.findByNames(containerFormMap);
			if (!ListUtils.isNotNull(list)) {
				String msg = "容器不存在，无法执行打包动作！！！";
				log.error(msg);
				throw new BusinessException(msg);
			}
			ContainerFormMap containerFormMap2 = list.get(0);
			containerFormMap2.put("container_status", -1);
			baseMapper.editEntity(containerFormMap2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
