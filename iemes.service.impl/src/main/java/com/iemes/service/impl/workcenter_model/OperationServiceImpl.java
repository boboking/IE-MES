package com.iemes.service.impl.workcenter_model;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.FlowStepFormMap;
import com.iemes.entity.ItemBomFormMap;
import com.iemes.entity.ItemFormMap;
import com.iemes.entity.OperationFormMap;
import com.iemes.entity.ProcessWorkFlowFormMap;
import com.iemes.entity.SfcStepFormMap;
import com.iemes.entity.UDefinedDataValueFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.BaseExtMapper;
import com.iemes.service.CommonService;
import com.iemes.service.workcenter_model.OperationService;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

@Service
public class OperationServiceImpl implements OperationService {

	@Inject
	private BaseExtMapper baseMapper;
	
	@Inject
	private CommonService commonService;
	
	private Logger log = Logger.getLogger(this.getClass());
	
	@Override
	@Transactional
	public void saveOperation(OperationFormMap operationFormMap) throws BusinessException {
		try {
			String id = operationFormMap.getStr("id");
			String operationNo = operationFormMap.getStr("operation_no");
			String version = operationFormMap.getStr("operation_version");
			
			if(StringUtils.isEmpty(operationNo)) {
				throw new BusinessException("操作编号不能为空，请输入！");
			}	
			if(StringUtils.isEmpty(version)) {
				throw new BusinessException("操作版本号不能为空，请输入！");
			}
			
			OperationFormMap operationFormMap2 = new OperationFormMap();
			operationFormMap2.put("operation_no", operationNo);
			operationFormMap2.put("operation_version", version);
			operationFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			List<OperationFormMap> list = baseMapper.findByNames(operationFormMap2);
			
			//如果编号存在，就执行更新，不存在就执行新增
			if (ListUtils.isNotNull(list)) {
				//更新 
				OperationFormMap operationFormMap3 = new OperationFormMap();
				operationFormMap3 = list.get(0);
				id = operationFormMap3.getStr("id");
				operationFormMap.set("id", id);
				
				baseMapper.editEntity(operationFormMap);
			}else {
				//新增
				id = UUIDUtils.getUUID();
				operationFormMap.set("id", id);
				baseMapper.addEntity(operationFormMap);
			}
			
			//保存自定义数据
			baseMapper.deleteByAttribute("data_type_detail_id", operationFormMap.getStr("id"), UDefinedDataValueFormMap.class);
			commonService.saveUDefinedDataValueS(operationFormMap.getStr("id"), operationFormMap.getStr("operationUdata"));
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("操作保存失败！"+e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delOperation(String operationId) throws BusinessException {
		try {
			//1.如果操作在工艺路线中，则不允许删除
			FlowStepFormMap flowStepFormMap = new FlowStepFormMap();
			flowStepFormMap.put("where", " where operation_id ='" + operationId + "' or next_operation_id ='"+ operationId +"'");
			List<FlowStepFormMap> listFlowStepFormMap = baseMapper.findByWhere(flowStepFormMap);
			if(ListUtils.isNotNull(listFlowStepFormMap)) {
				List<String> listBindFlowNo = new ArrayList<String>();
				String bindFlowNo = "";
				for(int i=0;i<listFlowStepFormMap.size();i++) {
					FlowStepFormMap map = listFlowStepFormMap.get(i);
				    List<ProcessWorkFlowFormMap> listProcessWorkFlowFormMap = baseMapper.findByAttribute
				    		("id", map.getStr("process_workflow_id"), ProcessWorkFlowFormMap.class);
				    if(!ListUtils.isNotNull(listProcessWorkFlowFormMap)) {
				    	throw new BusinessException("无法找到此操作所在的工艺路线Id " + map.getStr("process_workflow_id") + " 的详细信息，请联系系统管理员！");
				    }
				    ProcessWorkFlowFormMap processWorkFlowFormMap = listProcessWorkFlowFormMap.get(0);
				    String flowNo = processWorkFlowFormMap.getStr("process_workflow");
					if(StringUtils.isEmpty(bindFlowNo)) {
						listBindFlowNo.add(flowNo);
						bindFlowNo = flowNo;
					}else {
						if(!listBindFlowNo.contains(flowNo)) {
							listBindFlowNo.add(flowNo);
							bindFlowNo += "," + flowNo;
						}
					}
				}
				throw new BusinessException("此操作在工艺路线 " + bindFlowNo + " 中，无法执行删除！");
			}
		
			//2.如果操作在生产中，则不允许删除
			SfcStepFormMap sfcStepFormMap = new SfcStepFormMap();
			sfcStepFormMap.put("operation_id", operationId);
			List<SfcStepFormMap> listSfcStepFormMap= baseMapper.findByNames(sfcStepFormMap);
			if (ListUtils.isNotNull(listSfcStepFormMap)) {
				throw new BusinessException("此操作已用于生产，不可删除！");
			}

			//删除操作信息
			baseMapper.deleteByAttribute("id", operationId, OperationFormMap.class);
			
			//删除自定义数据信息
			baseMapper.deleteByAttribute("data_type_detail_id", operationId, UDefinedDataValueFormMap.class);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("操作删除失败！"+e.getMessage());
		}
	}

}
