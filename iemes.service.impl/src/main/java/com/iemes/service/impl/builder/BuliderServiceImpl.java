package com.iemes.service.impl.builder;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.PageSaveFormMap;
import com.iemes.entity.SiteFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.service.builider.BuliderService;
import com.iemes.util.ListUtils;
import com.iemes.util.UUIDUtils;

@Service
public class BuliderServiceImpl implements BuliderService {
	
	@Inject
	private BaseMapper baseMapper;
	
	private Logger log = Logger.getLogger(this.getClass());

	@Override
	@Transactional
	public void savePage(PageSaveFormMap pageSaveFormMap) throws BusinessException {
		try {
			String id = pageSaveFormMap.getStr("id");
            String pageName = pageSaveFormMap.getStr("page_name");
			
			if(StringUtils.isEmpty(pageName)) {
				throw new BusinessException("页面名称不能为空，请输入！");
			}			
			
			PageSaveFormMap pageSaveFormMap2 = new PageSaveFormMap();
			pageSaveFormMap2.put("page_name", pageName);
			List<PageSaveFormMap> listPageSaveFormMap = baseMapper.findByNames(pageSaveFormMap2);

			//如果编号存在，就执行更新，不存在就执行新增
			if (ListUtils.isNotNull(listPageSaveFormMap)) {
				//更新 
				PageSaveFormMap pageSaveFormMap3 = new PageSaveFormMap();
				pageSaveFormMap3 = listPageSaveFormMap.get(0);
				id = pageSaveFormMap3.getStr("id");
				pageSaveFormMap.set("id", id);
				
				baseMapper.editEntity(pageSaveFormMap);
			}else {
				//新增
				pageSaveFormMap.put("id", UUIDUtils.getUUID());
				baseMapper.addEntity(pageSaveFormMap);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("页面保存失败！" + e.getMessage());
		}
	}
}
