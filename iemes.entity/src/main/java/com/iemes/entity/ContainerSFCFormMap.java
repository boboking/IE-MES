package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;



/**
 * mds_container_sfc_relation实体表
 */
@TableSeg(tableName = "mds_container_sfc_relation", id="id")
public class ContainerSFCFormMap extends FormMap<String,Object>{

	/**
	 * 
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
