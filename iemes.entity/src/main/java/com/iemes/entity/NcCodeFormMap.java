package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;

/**
 * 扩展FormMap
 * @author Administrator
 *
 */
@TableSeg(tableName = "mds_nc_code", id="id")
public class NcCodeFormMap extends FormMap<String,Object>{

	private static final long serialVersionUID = 1L;

}
