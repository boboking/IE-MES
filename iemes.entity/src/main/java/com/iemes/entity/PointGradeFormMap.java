package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;



/**
 * ly_pod_function实体表
 */
@TableSeg(tableName = "ly_teaching_testinfo", id="id")
public class PointGradeFormMap extends FormMap<String,Object>{

	/**
	 *@descript
	 *@author MDS
	 *@version 2.0
	 */
	private static final long serialVersionUID = 1L;

}
