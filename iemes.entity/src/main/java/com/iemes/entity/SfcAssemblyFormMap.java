package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;

@TableSeg(tableName = "mds_sfc_assembly", id="id")
public class SfcAssemblyFormMap extends FormMap<String,Object>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
