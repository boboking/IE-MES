package com.iemes.service.system;

import com.iemes.entity.RoleFormMap;
import com.iemes.exception.BusinessException;

public interface RoleService {
	
	/**
	 * 保存角色
	 * @param roleFormMap
	 * @throws BusinessException
	 */
	void saveRole(RoleFormMap roleFormMap)throws BusinessException;

	/**
	 * 删除角色
	 * @param roleId
	 * @throws BusinessException
	 */
	void delRole(String roleId)throws BusinessException;
}
