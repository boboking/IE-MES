package com.iemes.service.workcenter_model;

import com.iemes.entity.SiteFormMap;
import com.iemes.entity.UserFormMap;
import com.iemes.exception.BusinessException;

public interface SiteService {
	/**
	 * 保存站点（新增或更新）
	 * @param siteFormMap
	 * @throws BusinessException
	 */
	void saveSite(SiteFormMap siteFormMap)throws BusinessException;

	/**
	 * 删除站点
	 * @param site
	 * @throws BusinessException
	 */
	void delSite(String id)throws BusinessException;
}
