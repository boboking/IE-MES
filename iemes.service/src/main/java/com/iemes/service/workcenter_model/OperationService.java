package com.iemes.service.workcenter_model;

import com.iemes.entity.OperationFormMap;
import com.iemes.exception.BusinessException;

public interface OperationService {

	/**
	 * 保存操作
	 * @param operationFormMap
	 * @throws BusinessException
	 */
	void saveOperation(OperationFormMap operationFormMap)throws BusinessException;
	
	/**
	 * 删除操作
	 * @param operationId
	 * @throws BusinessException
	 */
	void delOperation(String operationId)throws BusinessException;
}
