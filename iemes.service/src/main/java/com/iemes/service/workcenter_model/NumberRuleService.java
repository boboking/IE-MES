package com.iemes.service.workcenter_model;

import java.util.List;

import com.iemes.entity.NumberRuleFormMap;
import com.iemes.exception.BusinessException;

public interface NumberRuleService {
	
	/**
	 * 保存编号规则
	 * @param numberRuleFormMap
	 * @throws BusinessException
	 */
	void saveNumberRule(NumberRuleFormMap numberRuleFormMap)throws BusinessException;

	/**
	 * 删除编号规则
	 * @param ncCodeId
	 * @throws BusinessException
	 */
	void delNumberRule(String numberRuleId)throws BusinessException;	
	
	/**
	 * 按编号规则生成编号
	 * @param numberRuleFormMap 传入numberType
	 * @param numberCounts  生产的编号数
	 * @throws BusinessException
	 */
	List<String> generateNumberByNumberRule(NumberRuleFormMap numberRuleFormMap,int numberCounts,boolean isUpdateSeedNumber)throws BusinessException;
	
}
