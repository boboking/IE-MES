package com.iemes.service.workcenter_model;

import com.iemes.entity.ItemBomFormMap;
import com.iemes.exception.BusinessException;

public interface ItemBomService {
	
	/**
	 * 保存物料清单
	 * @param itemBomFormMap
	 * @throws BusinessException
	 */
	void saveItemBom(ItemBomFormMap itemBomFormMap)throws BusinessException;

	/**
	 * 删除物料清单
	 * @param itemBomId
	 * @throws BusinessException
	 */
	void delItemBom(String itemBomId)throws BusinessException;
}
