package com.iemes.service.workcenter_model;

import com.iemes.entity.UDefinedDataFormMap;
import com.iemes.exception.BusinessException;

public interface UserDefinedDataService {

	/**
	 * 保存自定义数据
	 * @param uDefinedDataFormMap
	 * @throws BusinessException
	 */
	void saveWorkCenter(UDefinedDataFormMap uDefinedDataFormMap)throws BusinessException;

	/**
	 * 删除自定义数据
	 * @param uDefinedDataId
	 * @throws BusinessException
	 */
	void delWorkCenter(String uDefinedDataId)throws BusinessException;
}
