package com.iemes.service.workcenter_model;

import java.util.List;

import com.iemes.entity.OperationFormMap;
import com.iemes.entity.ProcessWorkFlowFormMap;
import com.iemes.exception.BusinessException;

public interface ProcessWorkFlowService {

	/**
	 * 返回不同类型的操作集合
	 * @return
	 */
	List<OperationFormMap> getAllOperations()throws Exception;
	
	/**
	 * 保存工艺路线
	 * @throws BusinessException
	 */
	void saveProcessWorkFlow(ProcessWorkFlowFormMap processWorkFlowFormMap)throws BusinessException;
	
	/**
	 * 删除工艺路线
	 * @param processWorkflowId
	 * @throws BusinessException
	 */
	void delProcessWorkFlow(String processWorkflowId)throws BusinessException;
}
