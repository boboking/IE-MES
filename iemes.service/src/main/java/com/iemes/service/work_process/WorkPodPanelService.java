package com.iemes.service.work_process;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.builder.BuilderException;

import com.iemes.entity.FlowStepFormMap;
import com.iemes.entity.NcRepairFormMap;
import com.iemes.entity.SfcAssemblyFormMap;
import com.iemes.entity.SfcNcFormMap;
import com.iemes.entity.SfcStepFormMap;
import com.iemes.entity.ShopOrderSfcFormMap;
import com.iemes.entity.ShoporderFormMap;
import com.iemes.entity.WorkResourceFormMap;
import com.iemes.entity.WorkShopInventoryFormMap;
import com.iemes.exception.BusinessException;

import net.sf.json.JSONObject;

public interface WorkPodPanelService {

	/**
	 * 获取当前操作、资源上生产的SFC列表
	 * @param operationId
	 */
	List<Map<String, String>> getSfcOnOperation(String operationId)throws BusinessException;
	
	/**
	 * 获取当前操作、资源上排队的SFC列表
	 * @param operationId
	 * @param workResourceId
	 * @param sfc
	 * @return
	 * @throws BusinessException
	 */
	List<SfcStepFormMap> getSfcWaitOnOperation(String operationId, String workResourceId, String sfc)throws BusinessException;
	
	/**
	 * 根据面板ID获取按钮组
	 * @param podPanelId
	 * @return
	 * @throws BusinessException
	 */
	List<Map<String, String>> getButtonsByPodPanel(String podPanelId)throws BusinessException;
	
	/**
	 * 更新对象
	 * @param formMap
	 */
	void editFormMap(Object formMap)throws BusinessException;
	
	/**
	 * POD开始方法
	 * @param sfcStepFormMap	向上一条记录追加完成时间和资源ID
	 * @param sfcStepFormMap2	添加一条与向一条记录内容一致,但状态为生产中的记录
	 * @param shopOrderSfcFormMap	如果为首操作，修改sfc状态为生产中
	 * @throws BuilderException
	 */
	void podStart(SfcStepFormMap sfcStepFormMap, SfcStepFormMap sfcStepFormMap2, ShopOrderSfcFormMap shopOrderSfcFormMap)throws BuilderException;
	
	/**
	 * POD完成按钮方法
	 * @param data	行数据
	 * @param next_operation_id	next_operation_id
	 * @throws BusinessException
	 */
	void podFinish(JSONObject data, String next_operation_id)throws BusinessException;
	
	/**
	 * 获取装配列表
	 * @param map
	 * @return
	 * @throws BusinessException
	 */
	List<Map<String, Object>> getAssembleList(Map<String, String> map)throws BusinessException;
	
	/**
	 * POD完成绑定工单方法
	 * @param sfc	sfc
	 * @param shoporderId	
	 * @throws BusinessException
	 */
	void podBindShoporder(String bindSfc,ShoporderFormMap bindSoporderFormMap, FlowStepFormMap bindFistFlowStepFormMap,
			WorkResourceFormMap bindWorkResourceFormMap)throws BusinessException;
	
	/**
	 * 装配方法
	 * @param sfcStepFormMap
	 * @throws BusinessException
	 */
	void assembleItem(SfcAssemblyFormMap sfcAssemblyFormMap)throws BusinessException;
	
	/**
	 * 记录不良
	 * @param sfcNcFormMap	不良表插入记录
	 * @param sfcStepFormMap	追加当前步骤的完成时间
	 * @param sfcStepFormMap2	添加一条当前步骤完成记录
	 * @param sfcStepFormMap3	添加下一条维修操作记录
	 * @param ShopOrderSfcFormMap	修改sfc状态为维修中
	 * @throws BusinessException
	 */
	void recordNcCode(SfcNcFormMap sfcNcFormMap, SfcStepFormMap sfcStepFormMap, 
			SfcStepFormMap sfcStepFormMap2, SfcStepFormMap sfcStepFormMap3, ShopOrderSfcFormMap shopOrderSfcFormMap)throws BusinessException;
	
	/**
	 * 报废方法
	 * @param ncRepairFormMap
	 * @throws BusinessException
	 */
	void podScrap(SfcNcFormMap sfcNcFormMap, NcRepairFormMap ncRepairFormMap, SfcStepFormMap lastSfcStep, 
			SfcStepFormMap sfcStepFormMap, ShopOrderSfcFormMap shopOrderSfcFormMap, ShoporderFormMap lastSfcOnShoporder)throws BusinessException;
	
	/**
	 * 维修方法
	 * @param sfcNcFormMap
	 * @param ncRepairFormMap
	 * @param lastSfcStep
	 * @param sfcStepFormMap
	 * @param sfcStepFormMap2
	 * @throws BusinessException
	 */
	void podRepair(SfcNcFormMap sfcNcFormMap, NcRepairFormMap ncRepairFormMap, SfcStepFormMap lastSfcStep, 
			SfcStepFormMap sfcStepFormMap, SfcStepFormMap sfcStepFormMap2)throws BusinessException;
	
	/**
	 * 获取有效的装配物料批次
	 * @param param
	 * @return
	 */
	List<WorkShopInventoryFormMap> getValidItemBatch(Map<String, String> param);
}
