<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>UI快速开发界面</title>

<link rel="stylesheet" href="css/jquery_ui/jquery-ui.min.css">
<link rel="stylesheet" href="css/builder/builder.css" type="text/css" />

<script src="js/jquery_ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common/mds_builder.js"></script>
<script type="text/javascript" src="js/builder/builder.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/jpopup.js"></script>

</head>
<body>
	<div id="frame" class="frame">

		<!-- 该部分内容当样式调好后，会集成到mds-builder.js中去 -----start----------->
		<div class="canvas_left">
			<div class="menu_utils">
				<ul id="build_left" class="tabs_li_1tabs">
					<li><a href="#" tabid="control_div">控件</a></li>
				</ul>
				<div id="tab_centent">
					<div id="control_div">
						<!-- ********************************************* -->
						<ul class="operation_menu">
							<li><a href="#">快速布局</a>
								<ul>
									<li><a id="btn_addButtonToolbar" href="#">添加按钮栏</a></li>
									<li><a id="btn_addSearchButton" href="#">添加检索按钮</a></li>
									<li><a id="btn_addSaveButton" href="#">添加保存按钮</a></li>
									<li><a id="btn_addCleanButton" href="#">添加清除按钮</a></li>
									<li><a id="btn_addDelButton" href="#">添加删除按钮</a></li>
									<li><a id="btn_addButton" href="#">添加按钮</a></li>
									<li><a id="btn_addSearchDiv" href="#">添加检索区域</a></li>
									<li><a id="btn_addFormDiv" href="#">添加表单区域</a></li>
									<li><a href="#" id="btn_addTabs">添加tab页</a></li>
								</ul></li>
							<li><a href="#">组合控件</a>
								<ul>
									<li class="node label_input"><a href="#">label input</a></li>
									<li class="node label_search_input"><a href="#">label
											search_input</a></li>
									<li class="node label_search_input_ver"><a href="#">label
											search_input ver</a></li>
									<li class="node label_checkBox"><a href="#">label
											checkBox</a></li>
									<li class="node label_date"><a href="#">label date</a></li>
									<li class="node label_label"><a href="#">label label</a></li>
									<li class="node tabel"><a href="#">tabel</a></li>
									<li class="node choise_list"><a href="#">choise
											list</a></li>
								</ul></li>
							<li><a href="#">基础控件</a>
								<ul id="form_control">
									<li class="node button"><a href="#">按钮</a></li>
									<li class="node label"><a href="#">标签</a></li>
									<li class="node textbox"><a href="#">输入框</a></li>
									<li class="node checkbox"><a href="#">单选框</a></li>
									<li class="node dropdownlist"><a href="#">下拉框</a></li>
								</ul></li>
							<li class="menu_level_1"><a href="#">其它组件</a></li>
						</ul>
						<!-- ********************************************* -->
					</div>
				</div>
			</div>

			<div class="frame_properties">
				<ul id="build_properties" class="tabs_li_1tabs">
					<li><a href="#" tabid="properties">属性</a></li>
				</ul>
				<div id="centent">
					<div id="properties">
						
						<div>
							<label>控件名：</label>
							<input type="text" id="pro_tagName" readonly="readonly">
						</div>
						<div>
							<label>id：</label>
							<input type="text" id="pro_id">
						</div>
						<div>
							<label>name：</label>
							<input type="text" id="pro_name">
						</div>
						<div>
							<label>class：</label>
							<input type="text" id="pro_class">
						</div>
						<div>
							<label>text：</label>
							<input type="text" id="pro_text">
						</div>
						<div>
							<label>value：</label>
							<input type="text" id="pro_value">
						</div>
						<div>
							<label>url：</label>
							<input type="text" id="pro_url">
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 该部分内容当样式调好后，会集成到mds-builder.js中去 -----end----------->
	</div>
</body>
</html>