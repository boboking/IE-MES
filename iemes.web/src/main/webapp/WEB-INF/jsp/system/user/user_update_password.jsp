<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>密码重置</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/system/user/user_update_password.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>
		
		<div class="mds_row-50"></div>
		<div class="mds_container">
			<div class="mds_row">
				<div class="col-md-7 col-sm-7">
					<label class="must">旧密码:</label> 
					<input type="text" name="userFormMap.oldPassword" id="oldPassword"
						validate_allowedempty="N" validate_errormsg="旧密码不能为空！" /> 
				</div>
			</div>
			
			<div class="mds_row">
				<div class="col-md-7 col-sm-7">
					<label class="must">新密码:</label> 
					<input type="password" name="userFormMap.newPassword" id="newPassword"
						validate_allowedempty="N" validate_errormsg="新密码不能为空！" /> 
				</div>
			</div>
			
			<div class="mds_row">
				<div class="col-md-7 col-sm-7">
					<label class="must">确认新密码:</label> 
					<input type="password" name="userFormMap.newPassword2" id="newPassword2"
						validate_allowedempty="N" validate_errormsg="确认密码不能为空！" /> 
				</div>
			</div>
			<div class="mds_row-10"></div>
			<div class="mds_row">
				<div class="col-center" style="margin-left: 40px;">
					<input type="submit" value="修改">
					<input type="reset" value="清空">
				</div>
			</div>
		</div>
	</form>
</body>
</html>