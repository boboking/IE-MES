<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>		
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>自定义数据维护</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
<script type="text/javascript" src="js/workcenter_model/user_defined_data/user_defined_data_maintenance.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
		</div>
		
		<div class="hidden">
			<input type="hidden" value="${page_res_id}" id="page_res_id">
			<input type="hidden" value="${errorMessage}" id="errorMessage">
			<input type="hidden" value="${successMessage}" id="successMessage">
		</div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>

		<div class="inputForm_query">
			<div class="formField_query">
				<label class="must">站点:</label> <span class="formText_query">${site}</span>
			</div>
			<div class="formField_query">
				<label>类型:</label>
				<select class="formText_query" id="data_type" name="uDefinedDataFormMap.data_type">
					<c:forEach var="data" items="${data_list}">
						<option <c:if test="${data_type==data.key}">selected="selected" </c:if> value="${data.key}">${data.value }</option>
					</c:forEach>
				</select>
		   </div>
		</div>


		<!--  Tab -->
		<ul id="tabs" class="tabs_li_1tabs">
			<li><a href="#" tabid="tab1">自定义数据列表</a></li>
		</ul>
		<div id="content">
			<div id="tab1">
				<div class="bt_div">
					<a class="bt_a" id="tb_add">插入新行</a>
					<a class="bt_a" id="tb_del">删除选定行</a>
					<a class="bt_a" id="tb_delAll">删除全部</a>
				</div>
				<table class="mtable" id="udefinedTable">
					<tr>
						<th class="must">顺序</th>
						<th class="must">数据字段key</th>
						<th class="must">字段标签</th>
					</tr>
					<c:forEach var="key" items="${dataKeys}">
						<tr>
							<td><input type='text' validate_datatype='number_int' validate_errormsg='顺序必须为数字' value="${key.data_level}"></td>
							<td><input type='text' validate_allowedempty='N' validate_errormsg='数据字段key不能为空！'  value="${key.data_key}"></td>
							<td><input type='text' validate_allowedempty='N' validate_errormsg='字段标签不能为空！'  value="${key.data_label}"></td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>

	</form>
</body>
</html>