<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>		
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>生产操作员-操作</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
<script type="text/javascript" src="js/work_process/pod_operation/pod_operation.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
		<input type="button" id="btnQuery" value="检索"> 
		<input type="button" id="btnClear" value="清除"> 
	</div>
	
	<div class="hidden">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site}</span>
		</div>
		<div class="formField_query">
			<label class="must">生产操作员面板名称:</label> 
				<input type="text"
				dataValue="pod_panel_no" 
				viewTitle="POD面板"
				data-url="/popup/queryAllPodPanel.shtml" 
				class="formText_query" id="tbx_pod_panel_no" validate_allowedempty="N" validate_errormsg="请输入生产操作员面板名称！" 
				name="podPanelFormMap.pod_panel_no" value="${podPanelNo }"/>
			<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_pod_panel_no">
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_1tabs">
		<li><a href="#" tabid="tab1">操作面板</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<table class="mtable" id="pod_operation_table">
				<tr>
					<th data-value="pod_panel_no">POD面板编号</th>
					<th data-value="pod_panel_name">pod面板名称</th>
					<th data-value="pod_panel_desc">pod面板描述</th>
					<th data-value="status">状态</th>
					<th data-value="create_user">创建人</th>
					<th data-value="todo">操作</th>
				</tr>
				<c:forEach var="podPanelFormMap" items="${listPodPanelFormMap}">
					<tr>
						<td>${podPanelFormMap.pod_panel_no}</td>
						<td>${podPanelFormMap.pod_panel_name}</td>
						<td>${podPanelFormMap.pod_panel_desc}</td>
						<td>${podPanelFormMap.status}</td>
						<td>${podPanelFormMap.create_user}</td>
						<td><a href="#" data-value="${podPanelFormMap.id}" name="generatePodPanel">生成POD面板</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	
		</div>
	</div>

	<!--  Tab -->
	</form>
</body>
</html>