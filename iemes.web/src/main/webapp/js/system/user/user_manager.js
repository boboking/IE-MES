$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	var page_res_id = $('#page_res_id').val();
	//保存
	$("form").submit(function() {
		var data = $("form").serialize();
		
		var roles = $('#selectGroups')[0].children;
		var roleSelectGroups = "";
		for (var i=0;i<roles.length;i++) {
			roleSelectGroups += roles[i].value;
			if (i!=roles.length-1) roleSelectGroups += ","
		}
		data = data + "&userFormMap.roleSelectGroups="+roleSelectGroups;
		$.post(rootPath + "/system/user/saveUser.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage(data.message);
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	$('#btnResetPass').click(function (){
		var data = "userFormMap.id="+$('#user_id').val();
		$.post(rootPath + "/system/user/resetPass.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("重置成功，密码为：123456");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
	})
	
	//检索
	$('#btnQuery').click(function (){
		var account_name = $('#tbx_user_id').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/system/user/queryUser.shtml?account_name="+account_name+"&page_res_id="+page_res_id);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/system/user/list.shtml?page_res_id="+page_res_id);
	})
	
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("","该条数据将会被删除，确定继续吗？",function (){
			var user_id = $('#user_id').val();
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/system/user/delUser.shtml?user_id="+user_id+"&page_res_id="+page_res_id);
		});
	})
})