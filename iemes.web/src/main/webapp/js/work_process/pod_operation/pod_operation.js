$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	
	//检索
	$('#btnQuery').click(function (){
		var pod_panel_no = $('#tbx_pod_panel_no').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_process/pod_operation/queryPodPanel.shtml?pod_panel_no="+pod_panel_no);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_process/pod_operation/pod_operation.shtml");
	})
	
	//跳转到POD工作面板
	$("a[name='generatePodPanel']").click(function (e) {
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_process/work_pod_panel/work_pod_panel_maintenance.shtml?id="+$(this).data("value"));
	})
	
	//开始
	$('#startfun').click(function (e){
		var operation_id = $('#input_operation_id').val();
		var operation_no = $('#tbx_pod_operation').val();
		var resource_id = $('#input_resource_id').val();
		var resource_no = $('#tbx_pod_work_resource').val();
		var sfc = $('#tbx_sfc_no').val();
		var id = $('#pod_panel_id').val();
		
		var data = "&sfc="+sfc + "&id="+id + "&operation_id="+operation_id + "&resource_id="+resource_id
						+ "&operation_no="+operation_no + "&resource_no="+resource_no; 
		
		$.post(rootPath + "/work_process/work_pod_panel/podStart.shtml", data, function(e, status, xhr){
			var result= JSON.parse(e);
			if (result.status) {
				var tb = $(".index_centent");
				tb.empty();
				tb.load(rootPath + "/work_process/work_pod_panel/work_pod_panel_maintenance.shtml?id=" + id + data);
			}else {
				showErrorNoticeMessage(result.message);
			}
		},"json")
	});
	
	//完成
	$('#finishfun').click(function (e){
		var data = $.MTable.getCheckRowData("pod_controller_table");
		if (data==undefined || data.id=="") {
			showErrorNoticeMessage("请选择非空的行进行操作！！");
			return;
		}
		var pod_panel_id = $('#pod_panel_id').val();
		
		data = JSON.stringify(data);
		data = "data="+data + "&pod_panel_id=" + pod_panel_id;
		
		$.post(rootPath + "/work_process/work_pod_panel/getNextOperation.shtml", data, function(e, status, xhr){
			var result= JSON.parse(e);
			if (result.status) {
				var list = result.result;
				if (list.length==0) {
					doPodFinish(data);
				}else {
					checkNextOperation();
				}
			}else {
				showErrorNoticeMessage(result.message);
			}
		},"json")
	});
	
	//打开选择下一个操作的界面
	var checkNextOperation = function (data){
		data = JSON.stringify(data);
		var data_url = rootPath + "/work_process/work_pod_panel/checkNextOperation.shtml";
		var openwin = function (iWidth, iHeight){
			var itop = (window.screen.availHeight - 30 - iWidth) / 2;
			var ileft = (window.screen.availWidth - 10 - iHeight) / 2;
			window.open(data_url,'MDS IE-MES checkNextOperation',"height="+iHeight+", width="+iWidth+", top="+itop+", left="+ileft+
					",toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
		}
		openwin(300,350);
	}
	
	//刷新
	$('#pod_refresh').click(function (e){
		var id = $('#pod_panel_id').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_process/work_pod_panel/work_pod_panel_maintenance.shtml?id="+id);
	});
	
	//装配
	$('#assemblefun').click(function (e){
		$('#pod_assemble_list').empty();
		$('div.pod_assemble_list_panel').css({display : "none"});
		$('#pod_assemble_div').empty();
		$('div.pod_assemble_panel').css({display : "none"});
		
		var data = $.MTable.getCheckRowData("pod_controller_table");
		if (data==undefined || data.id=="") {
			showErrorNoticeMessage("请选择非空的行进行操作！！");
			return;
		}
		var pod_panel_id = $('#pod_panel_id').val();
		
		data = "param=assembleList&assembleList=" +JSON.stringify(data);
		
		$.post(rootPath + "/work_process/work_pod_panel/saveParam.shtml", data, function(e, status, xhr){
			var result= JSON.parse(e);
			if (result.status) {
				$.post(rootPath + "/work_process/work_pod_panel/checkShopOrderStatus.shtml", data, function(e, status, xhr){
					var result= JSON.parse(e);
					if (result.status) {
						$('#pod_assemble_list_label').text("装配物料清单");
						var pod_assemble_list = $('#pod_assemble_list');
						pod_assemble_list.empty();
						pod_assemble_list.load(rootPath + "/work_process/work_pod_panel/getAssembleItemBom.shtml");
						$('div.pod_assemble_list_panel').css({display : "block"});
					}else {
						showErrorNoticeMessage(result.message);
					}
				},"json")
			}else {
				showErrorNoticeMessage(result.message);
			}
		},"json");
	});
	
	//记录不良
	$('#nocodefun').click(function (e){
		$('#pod_assemble_list').empty();
		$('div.pod_assemble_list_panel').css({display : "none"});
		$('#pod_assemble_div').empty();
		$('div.pod_assemble_panel').css({display : "none"});
		
		var data = $.MTable.getCheckRowData("pod_controller_table");
		if (data==undefined || data.id=="") {
			showErrorNoticeMessage("请选择非空的行进行操作！！");
			return;
		}
		var pod_panel_id = $('#pod_panel_id').val();
		
		data = "param=nccode&nccode=" +JSON.stringify(data);
		
		$.post(rootPath + "/work_process/work_pod_panel/saveParam.shtml", data, function(e, status, xhr){
			var result= JSON.parse(e);
			if (result.status) {
				$('#pod_assemble_list_label').text("记录不良作业");
				var pod_assemble_list = $('#pod_assemble_list');
				pod_assemble_list.empty();
				pod_assemble_list.load(rootPath + "/work_process/work_pod_panel/recordNcCodeMaintenance.shtml");
				$('div.pod_assemble_list_panel').css({display : "block"});
			}else {
				showErrorNoticeMessage(result.message);
			}
		},"json")
	});
	
	//绑定工单
	$('#bindingshoporder').click(function (e){
		checkBindShoporder();
	});
	
	//维修
	$('#repairfun').click(function (e){
		$('#pod_assemble_list').empty();
		$('div.pod_assemble_list_panel').css({display : "none"});
		$('#pod_assemble_div').empty();
		$('div.pod_assemble_panel').css({display : "none"});
		
		var data = $.MTable.getCheckRowData("pod_controller_table");
		if (data==undefined || data.id=="") {
			showErrorNoticeMessage("请选择非空的行进行操作！！");
			return;
		}
		var pod_panel_id = $('#pod_panel_id').val();
		
		data = "param=repair&repair=" +JSON.stringify(data);
		
		$.post(rootPath + "/work_process/work_pod_panel/saveParam.shtml", data, function(e, status, xhr){
			var result= JSON.parse(e);
			if (result.status) {
				$('#pod_assemble_list_label').text("维修作业");
				var pod_assemble_list = $('#pod_assemble_list');
				pod_assemble_list.empty();
				pod_assemble_list.load(rootPath + "/work_process/work_pod_panel/repair_ui.shtml");
				$('div.pod_assemble_list_panel').css({display : "block"});
			}else {
				showErrorNoticeMessage(result.message);
			}
		},"json");
	});
	
	//报废
	$('#scrapfun').click(function (e){
		$('#pod_assemble_list').empty();
		$('div.pod_assemble_list_panel').css({display : "none"});
		$('#pod_assemble_div').empty();
		$('div.pod_assemble_panel').css({display : "none"});
		
		var data = $.MTable.getCheckRowData("pod_controller_table");
		if (data==undefined || data.id=="") {
			showErrorNoticeMessage("请选择非空的行进行操作！！");
			return;
		}
		var pod_panel_id = $('#pod_panel_id').val();
		
		data = "param=scrap&scrap=" +JSON.stringify(data);
		
		$.post(rootPath + "/work_process/work_pod_panel/saveParam.shtml", data, function(e, status, xhr){
			var result= JSON.parse(e);
			if (result.status) {
				$('#pod_assemble_list_label').text("报废作业");
				var pod_assemble_list = $('#pod_assemble_list');
				pod_assemble_list.empty();
				pod_assemble_list.load(rootPath + "/work_process/work_pod_panel/scrap_ui.shtml");
				$('div.pod_assemble_list_panel').css({display : "block"});
			}else {
				showErrorNoticeMessage(result.message);
			}
		},"json");
	});
	
	//关闭装配物料清单列表
	$('#bt_pod_assemble_list').click(function (){
		$('#pod_assemble_list').empty();
		$('div.pod_assemble_list_panel').css({display : "none"});
	})
	
	//关闭装配物料清单列表
	$('#bt_pod_assemble_panel').click(function (){
		$('#pod_assemble_div').empty();
		$('div.pod_assemble_panel').css({display : "none"});
	})
	//打开选择绑定工单的界面
	var checkBindShoporder = function (data){
		data = JSON.stringify(data);
		var data_url = rootPath + "/work_process/work_pod_panel/checkBindShoporder.shtml";
		var openwin = function (iWidth, iHeight){
			var itop = (window.screen.availHeight - 30 - iWidth) / 2;
			var ileft = (window.screen.availWidth - 10 - iHeight) / 2;
			window.open(data_url,'MDS IE-MES checkBindShoporder',"height="+iHeight+", width="+iWidth+", top="+itop+", left="+ileft+
					",toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
		}
		openwin(300,350);
	}
	
});

//执行完成功能
var doPodFinish = function (data){
	$.post(rootPath + "/work_process/work_pod_panel/podFinish.shtml", data, function(e, status, xhr){
		var data= JSON.parse(e);
		if (data.status) {
			var id = $('#pod_panel_id').val();
			
			var operation_id = $('#input_operation_id').val();
			var operation_no = $('#tbx_pod_operation').val();
			var resource_id = $('#input_resource_id').val();
			var resource_no = $('#tbx_pod_work_resource').val();
			var id = $('#pod_panel_id').val();
			
			var data = "&operation_id="+operation_id + "&resource_id="+resource_id
							+ "&operation_no="+operation_no + "&resource_no="+resource_no; 
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/work_process/work_pod_panel/work_pod_panel_maintenance.shtml?id="+id+data);
		}else {
			showErrorNoticeMessage(data.message);
		}
	},"json")
}

function getSfcOnOperation(row){
	var podPanelId = $('#pod_panel_id').val();
	var operation_id = row.operation_id;
	var operation_no = row.operation_no;
	
	var resource_id = row.resource_id;
	var resource_no = row.resource_no;
	
	if (operation_id==undefined || operation_id==null || operation_id == "") {
		return;
	}
	if (resource_id==undefined || resource_id==null || resource_id == "") {
		return;
	}
	$('#input_operation_id').val(operation_id);
	$('#input_resource_id').val(resource_id);
	$('#tbx_pod_operation').val(operation_no);
	$('#tbx_pod_work_resource').val(resource_no);
	var data = "operation_id="+operation_id + "&workResource_id="+resource_id + "&workResource_no=" + 
					resource_no + "&operation_no="+operation_no + "&podPanelId="+podPanelId;
	
	var tb = $(".index_centent");
	tb.empty();
	tb.load(rootPath + "/work_process/work_pod_panel/getSfcOnOperation.shtml?"+data);
}

function getWorkLineByResource(){
	var workResource_id = $('#input_resource_id').val();
	if (workResource_id==undefined || workResource_id==null || workResource_id=="") {
		showErrorNoticeMessage("资源不能为空！！！");
		return false;
	}
	var res = false;
	$.ajax({
		type : "post",
		url : rootPath + "/work_process/work_pod_panel/getWorkLineByResource.shtml",
		data : "workResource_id=" + workResource_id,
		async : false,
		success : function(data) {
			var rs= JSON.parse(data);
			if (rs instanceof Object) {
			}else {
				rs = JSON.parse(rs);
			}
			if (rs.status) {
				res = true;
			}else {
				showErrorNoticeMessage(rs.message);
			}
		}
	});
	return res;
}

//执行绑定工单功能
var doBindShoporder = function (data){
	var sfc = $('#tbx_sfc_no').val();
	var wordResourceId = $('#input_resource_id').val();
	var operationId = $('#input_operation_id').val();
	var path = rootPath + "/work_process/work_pod_panel/podBindShoporder.shtml?&sfc="+sfc+"&word_resource_id="+wordResourceId
	                    + "&operation_id="+operationId;
	
	$.post(path, data, function(e, status, xhr){
		var data= JSON.parse(e);
		if (data.status) {
			var id = $('#pod_panel_id').val();
			
			var operation_id = $('#input_operation_id').val();
			var operation_no = $('#tbx_pod_operation').val();
			var resource_id = $('#input_resource_id').val();
			var resource_no = $('#tbx_pod_work_resource').val();
			var id = $('#pod_panel_id').val();
			
			var data = "&operation_id="+operation_id + "&resource_id="+resource_id
							+ "&operation_no="+operation_no + "&resource_no="+resource_no; 
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/work_process/work_pod_panel/work_pod_panel_maintenance.shtml?id="+id+data);
		}else {
			showErrorNoticeMessage(data.message);
		}
	},"json")
}