$(document).ready(function() {
	
	//保存
	$("#scrap_form").submit(function() {
		var data = $("form").serialize();
		
		$.post(rootPath + "/work_process/work_pod_panel/podScrap.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("SFC已报废");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
});
