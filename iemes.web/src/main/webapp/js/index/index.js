$(document).ready(function(){
	$('.index_menu').empty();
	$('.index_menu').load("./getMenuList.shtml");
	
	$('.index_centent').load("welcome.jsp");
	
	//test
	//$('.index_centent').load("test.jsp");
	
	$('#main').click(function (){
		location.reload();
	});
	
	$('#logout').click(function (){
		$.MsgBox.Confirm("系统消息", "您将退出当前登录状态，确定继续吗？",function () {
			window.location = rootPath + "/logout.shtml";
		});  
	});
	
	$('#about').click(function (){
		$('.index_centent').empty();
		$('.index_centent').load("about.jsp");
	});
	
	$('#updatePass').click(function (){
		$(".index_subheader").find("span").text("修改密码");
		$('.index_centent').load(rootPath + "/system/user/updatePassView.shtml");
	})
})