$(document).ready(function() {
	//检索
	$('#btnQuery').click(function (){
		getReport();
	})	
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/report/workshop_inventory_report.shtml");
	})
})

function getReport(){
		var workshop_id = $("#input_workshop_id").val();
		if(workshop_id==null || workshop_id==""){
			showErrorNoticeMessage('车间不能为空');
			return;
		}
		$("#container").show();
		
		var dom = document.getElementById("container");
		var myChart = echarts.init(dom);
		myChart.showLoading();
		var app = {};
		option = null;	
		option = {
				//backgroundColor:'#B0C4DE',
				backgroundColor:{
					type: 'linear',
				    x: 0,
				    y: 0,
				    x2: 0,
				    y2: 1,
				    colorStops: [{
				        offset: 0, color: '#F0F0F0' // 0% 处的颜色
				    }, {
				        offset: 1, color: '#B0C4DE' // 100% 处的颜色
				    }],
				    globalCoord: false // 缺省为 false
				},
			    color: ['#8600df'],
			    tooltip : {
			        trigger: 'axis',
			        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
			            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
			        }
			    },				    
/*				legend : {
					data:['库存数量','安全库存数'],
					x : '80%', 
					y : '1%'
				},*/
			    title:{
			    	text:"车间库存报表",
			          x: '45%',
			          y : '1%'
			    	},
			    grid: {
			        left: '3%',
			        right: '4%',
			        bottom: '3%',
			        containLabel: true
			    },
			    xAxis : [
			        {
			            type : 'category',
			            data : [],
			            axisTick: {
			                alignWithLabel: true
			            }
			        }
			    ],
			    yAxis : [
			        {
			            type : 'value'
			        }
			    ],
			    series : [
			        {
			        	name:'库存数量',
			            type:'bar',
			            barWidth: '20%',
			            color: ['#7EC0EE'],
			            label:{
			            	normal:{show:true,position: 'top',color:'blue'}
			                  },
			            data:[]
			        }/*,
			        {
			        	name:'安全库存数',
			            type:'line',
			            color: ['#8968CD'],
			            linestytle:{
			            	normal:{
			            		width:10,
			            		type:'solid',
			            		shadowColor: 'rgba(0, 0, 0, 0.5)',
			            	    shadowBlur: 10,
			            	    shadowOffsetY : 5
			            	}
			            },
			            data:[]
			        }*/
			    ]
			};
		
		myChart.hideLoading();
		
		if (option && typeof option === "object") {
			myChart.setOption(option, true);
		}
		
		myChart.showLoading();
		
		var data = $("#mds_tab_form").serialize();
	    data = 'formMap.reportMethod=getInventoryData&formMap.workshop_id='+ workshop_id + '&' + data;
		$.ajax({
			type: 'POST', 
			data: data, 
			url: '/IE-MES/report/getReportData.shtml',
	        success: function (e,status) {
	        	 var data= JSON.parse(e);
	        	 data = JSON.parse(data);
	        	 if (data.status) {
	        		var newoption = myChart.getOption();
	    			var xAxisData = [];
	    			var yAxisData = [];
	    			var yAxisData2 = []; 
	        		for (var d in data.result){
	        			if(data.result[d].item_type == "purchase"){
		        			xAxisData.push(data.result[d].item_name);
		        			yAxisData.push(data.result[d].kc);
		        			/*yAxisData2.push(data.result[d].balance_down);*/
	        			}
	        		}

	    			//结存
	        		newoption.series[0].data=yAxisData;		        		
	    			newoption.xAxis[0].data = xAxisData;
	    			//安全数
	    			/*newoption.series[1].data=yAxisData2;*/
	    			
	         		myChart.hideLoading();
	        		myChart.setOption(newoption);
	        		
	        		showSuccessNoticeMessage("检索成功");

	        	}else {
	        		showErrorNoticeMessage("检索失败："+data.message);
	        	}
	        },
	        error: function (xhr,e) {
	        	showErrorNoticeMessage('请求失败,请联系管理员');
	        } 
	    });
	};

