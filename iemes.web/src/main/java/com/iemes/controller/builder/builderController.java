package com.iemes.controller.builder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.PageSaveFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.service.builider.BuliderService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

@Controller
@RequestMapping("/builder/builder/")
public class builderController extends BaseController {
	
	private String builderUrl = Common.BACKGROUND_PATH + "/builder/builder";
	private Logger log = Logger.getLogger(this.getClass());
	
	@Inject
	private BaseMapper baseMapper;
	
	@Inject
	private BuliderService buliderService;

	@RequestMapping("index")
	public String listUI(Model model,HttpServletRequest request) throws Exception {
		return builderUrl;
	}
	
	@RequestMapping("page_source_save")
	@ResponseBody
	@SystemLog(module="页面源保存",methods="页面源保存")//凡需要处理业务逻辑的.都需要记录操作日志
	public String PageSourceSave(Model model,HttpServletRequest request){
		PageSaveFormMap pageSaveFormMap = new PageSaveFormMap();
		pageSaveFormMap.put("create_time", DateUtils.getStringDateTime());
		pageSaveFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		
		try {
			String pageSourceInDraw = request.getParameter("page_source");
			
			//1.加上jsp文件的头尾
			String pageSourcePrefix = "<%@ page language=\"java\" contentType=\"text/html; charset=utf-8\" "
			                        + "pageEncoding=\"utf-8\"%>"
			                        + "<%@ taglib uri=\"http://java.sun.com/jsp/jstl/core\" prefix=\"c\"%>"		
			                        + "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">"
			                        + "<html>"
			                        + "<head>"
			                        + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">"
			                        + "<title>功能页面</title>"
			                        /*+ "<link rel=\"stylesheet\" href=\"css/jquery_ui/jquery-ui.min.css\">"
			                        + "<script src=\"js/jquery/jquery-3.2.1.min.js\"></script>"
			                        + "<script src=\"js/jquery_ui/jquery-ui.min.js\"></script>"*/
			                        + "<script type=\"text/javascript\" src=\"js/common/form_validate.js\"></script>"
			                        + "<script type=\"text/javascript\" src=\"js/common/jtab.js\"></script>"
			                        + "</head>"
			                        + "<body>";
			String pageSourcePostfix = "</body>"
                                     + "</html>";

			String pageSource = pageSourcePrefix + pageSourceInDraw + pageSourcePostfix;
			
			String pageName = request.getParameter("page_name");
			String pageResId = request.getParameter("page_res_id");
			
	
			pageSaveFormMap.put("page_res_id", pageResId);
			pageSaveFormMap.put("page_name", pageName);
			pageSaveFormMap.put("page_source", pageSource);
			pageSaveFormMap.put("page_source_in_draw", pageSourceInDraw);
			pageSaveFormMap.put("page_file_created", -1);
			
			buliderService.savePage(pageSaveFormMap);
		}catch(BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	
	//获取设计的页面源
	@ResponseBody
	@RequestMapping("query_page_source")
	public String query_page_source(Model model,HttpServletRequest request) throws Exception {
		PageSaveFormMap pageSaveFormMap = new PageSaveFormMap();
		String pageName = request.getParameter("page_name");
		pageSaveFormMap.put("page_name", pageName);
		List<PageSaveFormMap> list = baseMapper.findByNames(pageSaveFormMap);
		
		String pageSourceStr = "";
		String pageResId = "";
		if(ListUtils.isNotNull(list)) {
			pageSourceStr = list.get(0).getStr("page_source_in_draw");
			pageResId = list.get(0).getStr("page_res_id");
		}
		List<String> returnArray = new ArrayList<>();
		returnArray.add(pageSourceStr);
		returnArray.add(pageResId);
		return ResponseHelp.responseListToText(returnArray);
	}	

	
}
