package com.iemes.controller.builder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iemes.controller.index.BaseController;
import com.iemes.entity.PageSaveFormMap;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.util.Common;
import com.iemes.util.ListUtils;

@Controller
public class builderPageShowController extends BaseController {
	
	private String builderUrl = Common.BACKGROUND_PATH + "/builder/builder";
	private String buildPageShowUrl = Common.BACKGROUND_PATH + "/builder/builder_page_show";
	private Logger log = Logger.getLogger(this.getClass());
	
	@Inject
	private BaseMapper baseMapper;
	
	public String showUI(Model model,HttpServletRequest request,String url) throws Exception {
		//jsp页面，返回jsp路径,设计的页面，从数据库取值	
		String resId = getPara("id");
		PageSaveFormMap pageSaveFormMap = new PageSaveFormMap();
		pageSaveFormMap.put("page_res_id", resId);
		List<PageSaveFormMap> list = baseMapper.findByNames(pageSaveFormMap);
		if(ListUtils.isNotNull(list)) {
			PageSaveFormMap map = list.get(0);
			model.addAttribute("builder_page", map);
			return buildPageShowUrl;
		}else {
			return url;
		}
	}

	@RequestMapping("/builder/builder/bulider_page_show")
	public String listUI(Model model,HttpServletRequest request) throws Exception {
		return showUI(model,request,builderUrl);
	}
	
	@RequestMapping("/builder/builder/generateJspCode")
	public void generateJspCode(HttpServletRequest request, HttpServletResponse response) {
		
		PageSaveFormMap pageSaveFormMap = new PageSaveFormMap();
		String pageName = request.getParameter("page_name");
		pageSaveFormMap.put("page_name", pageName);
		List<PageSaveFormMap> list = baseMapper.findByNames(pageSaveFormMap);
		PageSaveFormMap pageSaveFormMap2 = list.get(0);
		String fileName = pageName + ".jsp";
		
		String pageSource = pageSaveFormMap2.getStr("page_source");
		byte[] data = pageSource.getBytes();
		
		if (fileName != null) {
            response.setContentType("application/force-download");// 设置强制下载不打开
            response.addHeader("Content-Disposition",
                    "attachment;fileName=" + fileName);// 设置文件名
            OutputStream os = null;
			try {
				os = response.getOutputStream();
				os.write(data, 0, data.length);
	            os.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				 try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
        }
	}
	
	@RequestMapping("/builder/builder/generateJsCode")
	public void generateJsCode(HttpServletRequest request, HttpServletResponse response) {
        ServletOutputStream os = null;
        FileInputStream in = null;
		try {
			os = response.getOutputStream();
			String path = request.getServletContext().getRealPath("/template/");
	        String fileName = "template.js";
	        File file = new File(path + fileName);
	        
	        response.setContentType("application/force-download");// 设置强制下载不打开
	        response.addHeader("Content-Disposition",
	                "attachment;fileName=" + fileName);// 设置文件名
	        
	        in = new FileInputStream(file);
	        byte[] data = new byte[(int) file.length()];
	        in.read(data);
	        os.write(data);
	        os.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				in.close();
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
