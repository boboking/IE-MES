package com.iemes.controller.work_process;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.PodButtonFormMap;
import com.iemes.entity.PodFunctionFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.service.work_process.PodButtonService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

@Controller
@RequestMapping("/work_process/pod_button/")
public class PodButtonController extends BaseController {

	private Logger log = Logger.getLogger(this.getClass());
	
	@Inject
	private PodButtonService podButtonService;
	
	private final String podButtonMaintenanceUrl = Common.BACKGROUND_PATH + "/work_process/pod_button/pod_button_maintenance";
	
	@RequestMapping("pod_button_maintenance")
	public String pod_button_maintenance(Model model) throws Exception {
		return podButtonMaintenanceUrl;
	}
	
	@ResponseBody
	@RequestMapping("savePodButton")
	@SystemLog(module="生产过程管理",methods="生产操作员-按钮-保存生产操作员-按钮")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String savePodButton(Model model,HttpServletRequest request){
		PodButtonFormMap podButtonFormMap = getFormMap(PodButtonFormMap.class);
		podButtonFormMap.put("create_time", DateUtils.getStringDateTime());
		podButtonFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		podButtonFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		try {
			podButtonService.savePodButton(podButtonFormMap);
		}catch(BusinessException e) {
			log.error(e.getMessage());
			model.addAttribute(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	
	/**
	 * 检索生产操作员-按钮
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryPodButton")
	public String queryPodButton(Model model,HttpServletRequest request) {
		String podFunctionNo = request.getParameter("pod_button_no");
		try {
			//查询生产操作员-按钮
			PodButtonFormMap podButtonFormMap = new PodButtonFormMap();
			podButtonFormMap.put("pod_button_no", podFunctionNo);
			podButtonFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			List<PodButtonFormMap> list = baseMapper.findByNames(podButtonFormMap);
			
			if (!ListUtils.isNotNull(list)) {
				log.error("未检索到生产操作员-按钮编号编号为："+podFunctionNo.toUpperCase()+"的信息");
				model.addAttribute("errorMessage","未检索到生产操作员-按钮编号为："+podFunctionNo.toUpperCase()+"的信息");
				return podButtonMaintenanceUrl;
			}
			PodButtonFormMap podButtonFormMap2 = list.get(0);
			
			List<PodFunctionFormMap> listPodFunctionFormMap = baseMapper.findByAttribute("id", podButtonFormMap2.getStr("pod_function"), PodFunctionFormMap.class);
			if (!ListUtils.isNotNull(listPodFunctionFormMap)) {
				log.error("未检索到对应的触发器信息");
				model.addAttribute("errorMessage","未检索到对应的触发器信息");
				return podButtonMaintenanceUrl;
			}
			PodFunctionFormMap podFunctionFormMap = listPodFunctionFormMap.get(0);
			model.addAttribute("podFunctionFormMap", podFunctionFormMap);
			model.addAttribute("podButtonFormMap", podButtonFormMap2);
		}catch(Exception e) {
			log.error(e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			return podButtonMaintenanceUrl;
		}
		model.addAttribute("successMessage", "检索成功");
		return podButtonMaintenanceUrl;
	}
	
	/**
	 * 删除生产操作员-按钮
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("delPodButton")
	public String delPodButton(Model model,HttpServletRequest request) {
		String podFunctionId = request.getParameter("pod_button_id");
		try {
			if (StringUtils.isEmpty(podFunctionId)) {
				throw new BusinessException("操作失败，参数不能为空！");
			}
			podButtonService.delPodButton(podFunctionId);
		}catch(BusinessException e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", e.getMessage());
			return podButtonMaintenanceUrl;
		}
		model.addAttribute("successMessage", "删除成功");
		return podButtonMaintenanceUrl;
	}
}
