package com.iemes.controller.work_process;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iemes.controller.index.BaseController;
import com.iemes.entity.ShopOrderSfcFormMap;
import com.iemes.entity.ShoporderFormMap;
import com.iemes.util.Common;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;

@Controller
@RequestMapping("/work_process/barcode_printing/")
public class BarcodePrintController extends BaseController {

	private Logger log = Logger.getLogger(this.getClass());
	
	private final String barcodePrintUrl = Common.BACKGROUND_PATH + "/work_process/barcode_printing/barcode_printing";
	
	@RequestMapping("barcode_printing")
	public String barcode_printing(Model model, HttpServletRequest request) {
		return barcodePrintUrl;
	}
	
	/**
	 * 检索工单SFC信息
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryShopOrderSfcList")
	public String queryShopOrderSfcList(Model model,HttpServletRequest request) {
		String shopOrderNo = request.getParameter("shop_order_no");
		String workcenterId = request.getParameter("workcenter_id");
		String workLineId = request.getParameter("workline_id");
		String workcenterNo = request.getParameter("workcenter_no");
		String worklineNo = request.getParameter("workline_no");
		try {
			ShoporderFormMap shoporderFormMap = new ShoporderFormMap();
			shoporderFormMap.put("shoporder_no", shopOrderNo);
			shoporderFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			List<ShoporderFormMap> list = baseMapper.findByNames(shoporderFormMap);
			
			if (!ListUtils.isNotNull(list)) {
				log.error("未检索到工单编号编号为："+shopOrderNo.toUpperCase()+"的信息");
				model.addAttribute("errorMessage","未检索到工单编号为："+shopOrderNo.toUpperCase()+"的信息");
				return barcodePrintUrl;
			}
			ShoporderFormMap shoporderFormMap2 = list.get(0);
			
			//查工单下的SFC列表
			String id = shoporderFormMap2.getStr("id");
			if (!StringUtils.isEmpty(id)) {
				
				ShopOrderSfcFormMap shopOrderSfcFormMap = new ShopOrderSfcFormMap();
				shopOrderSfcFormMap.put("shoporder_id", id);
				shopOrderSfcFormMap.put("workcenter_id", workcenterId);
				shopOrderSfcFormMap.put("workline_id", workLineId);
				List<ShopOrderSfcFormMap> shopOrderSfcFormMapList = baseMapper.findByNames(shopOrderSfcFormMap);
				if (ListUtils.isNotNull(shopOrderSfcFormMapList)) {
					model.addAttribute("shopOrderSfcFormMapList", shopOrderSfcFormMapList);
				}
			}
			model.addAttribute("shoporderFormMap", shoporderFormMap2);
			model.addAttribute("workcenterId", workcenterId);
			model.addAttribute("workLineId", workLineId);
			model.addAttribute("workcenterNo", workcenterNo);
			model.addAttribute("worklineNo", worklineNo);
		}catch(Exception e) {
			log.error(e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			return barcodePrintUrl;
		}
		model.addAttribute("successMessage", "检索成功");
		return barcodePrintUrl;
	}
}
