package com.iemes.controller.packing;


import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.ContainerFormMap;
import com.iemes.entity.ContainerSFCFormMap;
import com.iemes.entity.ContainerTypeFormMap;
import com.iemes.entity.NumberRuleFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.service.PackingService;
import com.iemes.service.impl.workcenter_model.NumberRuleServiceImpl;
import com.iemes.service.workcenter_model.NumberRuleService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

/**
 * 
 * @author mds
 * @Email: mds
 * @version 2.0v
 */
@Controller
@RequestMapping("/packing/")
public class PackingController extends BaseController {
	
	private Logger log = Logger.getLogger(this.getClass());
	
	@Inject
	private PackingService packingService;
	
	@Inject
	private NumberRuleService numberRuleService;
	
	private String packingManagerUrl = Common.BACKGROUND_PATH + "/packing/packing_manager/packing_manager";
	
	//页面跳转
	@RequestMapping("packing_manager")
	public String Get_packing_manager_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return packingManagerUrl;
	}
	
	/**
	 * 保存容器和SFC关系
	 * @return
	 */
	@ResponseBody
	@RequestMapping("saveContainerSFC")
	@SystemLog(module="包装管理",methods="包装管理-保存")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String saveContainerSFC() {
		ContainerTypeFormMap containerFormMap = getFormMap(ContainerTypeFormMap.class);
		containerFormMap.put("create_time", DateUtils.getStringDateTime());
		containerFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		containerFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		
		try {
			packingService.saveContainerSFC(containerFormMap);
		} catch (BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	
	/**
	 * 包装
	 * @return
	 */
	@ResponseBody
	@RequestMapping("packing")
	@SystemLog(module="包装管理",methods="包装")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String packing() {
		ContainerSFCFormMap containerSFCFormMap = getFormMap(ContainerSFCFormMap.class);
		try {
			containerSFCFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			List<ContainerSFCFormMap> list = baseMapper.findByNames(containerSFCFormMap);
			if (ListUtils.isNotNull(list)) {
				String msg = containerSFCFormMap.getStr("wrappage") + "已存在容器中，不允许重复包装！！！";
				log.error(msg);
				throw new BusinessException(msg);
			}
			String message = packingService.packing(containerSFCFormMap);
			return ResponseHelp.responseText(message);
		} catch (BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
	}
	
	/**
	 * 移除
	 * @return
	 */
	@ResponseBody
	@RequestMapping("unPacking")
	@SystemLog(module="包装管理",methods="移除")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String unPacking() {
		ContainerSFCFormMap containerSFCFormMap = getFormMap(ContainerSFCFormMap.class);
		try {
			packingService.unPacking(containerSFCFormMap);
			return ResponseHelp.responseText();
		} catch (BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
	}
	
	/**
	 * 检索容器中存放的SFC和容器基本信息
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryPackContainer")
	public String queryContainer(Model model, HttpServletRequest request) {
		String siteId = ShiroSecurityHelper.getSiteId();
		String containerTypeNo = request.getParameter("container_type_no");
		String container_id = request.getParameter("container_id");
		model.addAttribute("container_id", container_id);
		
		//1.查询容器类型的基本信息
		ContainerTypeFormMap containerTypeFormMap = new ContainerTypeFormMap();
		containerTypeFormMap.put("container_type_no", containerTypeNo);
		containerTypeFormMap.put("site_id", siteId);
		List<ContainerTypeFormMap> listContainerFormMap = baseMapper.findByNames(containerTypeFormMap);
		if (!ListUtils.isNotNull(listContainerFormMap)) {
			String errMessage = "未检索到容器类型编号为：" + containerTypeNo.toUpperCase() + "的信息";
			log.error(errMessage);
			model.addAttribute("errorMessage", errMessage);
			return packingManagerUrl;
		}
		containerTypeFormMap = listContainerFormMap.get(0);	
		
		//2.查询容器信息
		ContainerFormMap containerFormMap = new ContainerFormMap();
		containerFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		containerFormMap.put("container_id", container_id);
		List<ContainerFormMap> list = baseMapper.findByNames(containerFormMap);
		if (ListUtils.isNotNull(list)) {
			ContainerFormMap containerFormMap2 = list.get(0);
			model.addAttribute("containerFormMap", containerFormMap2);
		}
		
		//3.查询容器中装SFC信息
		ContainerSFCFormMap containerSFCFormMap = new ContainerSFCFormMap();
		containerSFCFormMap.put("container_id", container_id);
		containerSFCFormMap.put("site_id", siteId);
		List<ContainerSFCFormMap> listContainerSFCFormMap = baseMapper.findByNames(containerSFCFormMap);
		int sfcCount = listContainerSFCFormMap.size();
		
		if(ListUtils.isNotNull(listContainerSFCFormMap)){
			model.addAttribute("listContainerSFCFormMap", listContainerSFCFormMap);
		}
		
		model.addAttribute("containerTypeFormMap", containerTypeFormMap);
		model.addAttribute("sfcCount", sfcCount);
		model.addAttribute("successMessage", "检索成功");
		return packingManagerUrl;
	}
	
	/**
	 * 获取新的容器编号
	 * @return
	 */
	@ResponseBody
	@RequestMapping("getContainerId")
	@SystemLog(module="包装管理",methods="包装管理-获取容器编号")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String getContainerId(Model model) {
		try {
			ContainerTypeFormMap containerTypeFormMap = getFormMap(ContainerTypeFormMap.class);
			String number_rule_id = containerTypeFormMap.getStr("number_rule_id");

			// 获取自动生成的编号
			NumberRuleFormMap numberRuleFormMap = new NumberRuleFormMap();
			numberRuleFormMap.put("id", number_rule_id);
			List<String> listSfcNo = numberRuleService.generateNumberByNumberRule(numberRuleFormMap, 1,true);
			
			numberRuleFormMap = NumberRuleServiceImpl.GetNumberRuleFormMapAfterHandle();

			if (!ListUtils.isNotNull(listSfcNo)) {
				String msg = "编号生成失败，请联系管理员！！！";
				log.error(msg);
				return ResponseHelp.responseErrorText(msg);
			}
			String number_rule = listSfcNo.get(0);
			return ResponseHelp.responseText(number_rule);
		} catch (BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
	}
	
	/**
	 * 解包（修改容器状态）
	 * @return
	 */
	@ResponseBody
	@RequestMapping("openContainer")
	@SystemLog(module="包装管理",methods="解包")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String openContainer(HttpServletRequest request) {
		try {
			ContainerFormMap containerFormMap = getFormMap(ContainerFormMap.class);
			containerFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			packingService.openContainer(containerFormMap);
			return ResponseHelp.responseText();
		} catch (BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
	}
	
	/**
	 * 打包（修改容器状态）
	 * @return
	 */
	@ResponseBody
	@RequestMapping("closeContainer")
	@SystemLog(module="包装管理",methods="打包")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String closeContainer(HttpServletRequest request) {
		try {
			ContainerFormMap containerFormMap = getFormMap(ContainerFormMap.class);
			containerFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			packingService.closeContainer(containerFormMap);
			return ResponseHelp.responseText();
		} catch (BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
	}
}