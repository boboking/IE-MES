package com.iemes.controller.workcenter_model;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.OperationFormMap;
import com.iemes.entity.WorkResourceFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.service.workcenter_model.OperationService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

@Controller
@RequestMapping("/workcenter_model/operation/")
public class OperationController extends BaseController {
	
	@Inject
	private OperationService operationService;

	private Logger log = Logger.getLogger(this.getClass());
	
	private final String operationMaintenanceUrl = Common.BACKGROUND_PATH + "/workcenter_model/operation/operation_maintenance";
	
	@RequestMapping("operation_maintenance")
	public String operation_maintenance(Model model, HttpServletRequest request) {
		model.addAttribute("dataKeys", getUDefinedDataField("operation_maintenance"));
		handlePageRes(model,request);
		return operationMaintenanceUrl;
	}
	
	/**
	 * 保存操作
	 * @param model
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("saveOperation")
	@SystemLog(module="车间基础建模",methods="操作-保存操作")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String saveOperation(Model model,HttpServletRequest request){
		OperationFormMap operationFormMap = getFormMap(OperationFormMap.class);
		operationFormMap.put("create_time", DateUtils.getStringDateTime());
		operationFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		operationFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		try {
			operationService.saveOperation(operationFormMap);
		}catch(BusinessException e) {
			log.error(e.getMessage());
			model.addAttribute(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	
	/**
	 * 检索操作
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryOperation")
	public String queryOperation(Model model,HttpServletRequest request) {
		String operation_no = request.getParameter("operation_no");
		String version = request.getParameter("version");
		try {
			//查询操作信息
			OperationFormMap operationFormMap = new OperationFormMap();
			operationFormMap.put("operation_no", operation_no);
			if (!StringUtils.isEmpty(version)) {
				operationFormMap.put("operation_version", version);
			}
			operationFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			operationFormMap.put("orderby", " order by operation_version");
			List<OperationFormMap> list = baseMapper.findByNames(operationFormMap);
			
			if (!ListUtils.isNotNull(list)) {
				log.error("未检索到操作编号编号为："+operation_no.toUpperCase()+"的信息");
				model.addAttribute("errorMessage","未检索到操作编号为："+operation_no.toUpperCase()+"的信息");
				handlePageRes(model,request);
				return operationMaintenanceUrl;
			}
			OperationFormMap operationFormMap2 = list.get(0);
			model.addAttribute("operation", operationFormMap2);
			
			//查询自定义数据信息
			getUDefinedDataValue(model,operationFormMap2.getStr("id"),"operation_maintenance","dataKeys");
			
			//查询资源信息
			if (!StringUtils.isEmpty(operationFormMap2.getStr("default_resource"))) {
				List<WorkResourceFormMap> list2 = baseMapper.findByAttribute("id", operationFormMap2.getStr("default_resource"), WorkResourceFormMap.class);
				if (!ListUtils.isNotNull(list2)) {
					log.error("未检索到默认资源的相关信息，请确定该数据是否正确或被删除");
					model.addAttribute("errorMessage","未检索到默认资源的相关信息，请确定该数据是否正确或被删除");
					handlePageRes(model,request);
					return operationMaintenanceUrl;
				}
				model.addAttribute("resource", list2.get(0));
			}
			
		}catch(Exception e) {
			log.error(e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			handlePageRes(model,request);
			return operationMaintenanceUrl;
		}
		model.addAttribute("successMessage", "检索成功");
		handlePageRes(model,request);
		return operationMaintenanceUrl;
	}
	
	/**
	 * 删除操作
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("delOperation")
	public String delOperation(Model model,HttpServletRequest request) {
		String operation_id = request.getParameter("operation_id");
		try {
			if (StringUtils.isEmpty(operation_id)) {
				throw new BusinessException("操作失败，参数不能为空！");
			}
			//TODO 
			operationService.delOperation(operation_id);
		}catch(BusinessException e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", e.getMessage());
			handlePageRes(model,request);
			return operationMaintenanceUrl;
		}
		model.addAttribute("successMessage", "删除成功");
		handlePageRes(model,request);
		return operationMaintenanceUrl;
	}
}
