package com.iemes.controller.workcenter_model;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.controller.index.BaseController;
import com.iemes.entity.OperationFormMap;
import com.iemes.entity.ProcessWorkFlowFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.service.workcenter_model.ProcessWorkFlowService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

@Controller
@RequestMapping("/workcenter_model/process_workflow/")
public class ProcessWorkFlowController extends BaseController{
	
	@Inject
	private ProcessWorkFlowService processWorkFlowService;
	
	private Logger log = Logger.getLogger(this.getClass());
	
	private String processWorkFlowUrl = Common.BACKGROUND_PATH + "/workcenter_model/process_workflow/process_workflow";
	
	/**
	 * 获取操作
	 * @param model
	 * @throws Exception 
	 */
	private void getOperations(Model model) throws Exception {
			List<OperationFormMap> listAllOperations = processWorkFlowService.getAllOperations();
			
			List<OperationFormMap> comonOperations = new ArrayList<OperationFormMap>();
			List<OperationFormMap> repairOperations = new ArrayList<OperationFormMap>();
			List<OperationFormMap> testOperations = new ArrayList<OperationFormMap>();
			for (int i=0;i<listAllOperations.size();i++) {
				OperationFormMap operationFormMap = listAllOperations.get(i);
				String operationType = operationFormMap.getStr("operation_type");
				if ("normal".equals(operationType)) {
					comonOperations.add(operationFormMap);
				}else if ("repair".equals(operationType)) {
					repairOperations.add(operationFormMap);
				}else if ("test".equals(operationType)){
					testOperations.add(operationFormMap);
				}
			}
			model.addAttribute("common", comonOperations);
			model.addAttribute("repair", repairOperations);
			model.addAttribute("test", testOperations);
	}
	
	/**
	 * 跳转到工艺路线界面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("process_workflow")
	public String process_workflow_index(Model model, HttpServletRequest request) {
		try {
			getOperations(model);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		handlePageRes(model,request);
		return processWorkFlowUrl;
	}
	
	/**
	 * 保存工艺路线
	 * @param model
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("saveProcessWrokFlow")
	public String saveProcessWrokFlow(Model model, HttpServletRequest request) {
		ProcessWorkFlowFormMap processWorkFlowFormMap = getFormMap(ProcessWorkFlowFormMap.class);
		processWorkFlowFormMap.put("create_time", DateUtils.getStringDateTime());
		processWorkFlowFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		processWorkFlowFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		processWorkFlowFormMap.put("data", processWorkFlowFormMap.getStr("mdata"));
		
		try {
			processWorkFlowService.saveProcessWorkFlow(processWorkFlowFormMap);
		}catch(BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	
	/**
	 * 检索工艺路线
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryProcessWorkFlow")
	public String queryProcessWorkFlow(Model model,HttpServletRequest request) {
		String processWorkflow = request.getParameter("process_workflow");
		String version = request.getParameter("version");
		try {
			//查询工艺路线信息
			ProcessWorkFlowFormMap processWorkFlowFormMap = new ProcessWorkFlowFormMap();
			processWorkFlowFormMap.put("process_workflow", processWorkflow);
			if (!StringUtils.isEmpty(version)) {
				processWorkFlowFormMap.put("process_workflow_version", version);
			}
			processWorkFlowFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			processWorkFlowFormMap.put("orderby", " order by process_workflow_version");
			List<ProcessWorkFlowFormMap> list = baseMapper.findByNames(processWorkFlowFormMap);
			
			getOperations(model);
			if (!ListUtils.isNotNull(list)) {
				log.error("未检索到工艺路线编号为："+processWorkflow.toUpperCase()+"的信息");
				model.addAttribute("errorMessage","未检索到工艺路线编号为："+processWorkflow.toUpperCase()+"的信息");
				handlePageRes(model,request);
				return processWorkFlowUrl;
			}
			ProcessWorkFlowFormMap processWorkFlowFormMap2 = list.get(0);
			String data = processWorkFlowFormMap2.getStr("data");
			data = data.replaceAll("\"", "'");
			processWorkFlowFormMap2.set("data", data);
			model.addAttribute("process_workflow", processWorkFlowFormMap2);
		}catch(Exception e) {
			log.error(e.getMessage());
			try {
				getOperations(model);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			model.addAttribute("errorMessage", e.getMessage());
			handlePageRes(model,request);
			return processWorkFlowUrl;
		}
		model.addAttribute("successMessage", "检索成功");
		handlePageRes(model,request);
		return processWorkFlowUrl;
	}
	
	/**
	 * 删除工艺路线
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("delProcessWorkFlow")
	public String delProcessWorkFlow(Model model,HttpServletRequest request) {
		String processWorkflowId = request.getParameter("process_workflow_id");
		try {
			if (StringUtils.isEmpty(processWorkflowId)) {
				throw new BusinessException("操作失败，参数不能为空！");
			}
			processWorkFlowService.delProcessWorkFlow(processWorkflowId);
			getOperations(model);
		}catch(Exception e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", e.getMessage());
			try {
				getOperations(model);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			handlePageRes(model,request);
			return processWorkFlowUrl;
		}
		model.addAttribute("successMessage", "删除成功");
		handlePageRes(model,request);
		return processWorkFlowUrl;
	}
	
}
