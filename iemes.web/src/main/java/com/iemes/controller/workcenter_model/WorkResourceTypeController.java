package com.iemes.controller.workcenter_model;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.WorkResourceFormMap;
import com.iemes.entity.WorkResourceTypeFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.service.workcenter_model.WorkResourceTypeService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

/**
 * 
 */
@Controller
@RequestMapping("/workcenter_model/work_resource_type/")
public class WorkResourceTypeController extends BaseController {
	
	@Inject
	private BaseMapper baseMapper;
	
	@Inject
	private WorkResourceTypeService workResourceTypeService;
	
	private Logger log = Logger.getLogger(this.getClass());
	private String workResourceTypeMaintenanceUrl = Common.BACKGROUND_PATH + "/workcenter_model/work_resource_type/work_resource_type_maintenance";

	//页面跳转
	@RequestMapping("work_resource_type_maintenance")
	public String Get_work_resource_type_maintenance_Url(Model model,HttpServletRequest request) throws Exception {
		model.addAttribute("res", findByRes());
		Session session = SecurityUtils.getSubject().getSession();
		String site = session.getAttribute("site").toString();
		model.addAttribute("site", site);
		String siteId = ShiroSecurityHelper.getSiteId();
		model.addAttribute("siteId", siteId);
		//获取资源分配列表
		getWorkResourceBindData(model,siteId,"");
		handlePageRes(model,request);
		return workResourceTypeMaintenanceUrl;
	}
	
	private void getWorkResourceBindData(Model model, String siteId, String workResourceTypeId) {
		// 1.已分配资源类型的资源列表
		List<WorkResourceFormMap> listSelectedWorkResourceFormMap = new ArrayList<WorkResourceFormMap>();
		if (!StringUtils.isEmpty(workResourceTypeId)) {
			WorkResourceFormMap workResourceFormMap1 = new WorkResourceFormMap();
			workResourceFormMap1.put("site_id", siteId);
			workResourceFormMap1.put("resource_type_id", workResourceTypeId);
			listSelectedWorkResourceFormMap = baseMapper.findByNames(workResourceFormMap1);
		}
		// 2.未分配资源类型的资源列表
		WorkResourceFormMap workResourceFormMap2 = new WorkResourceFormMap();
		workResourceFormMap2.put("where", " where site_id ='"+ siteId +"' and (resource_type_id is null || trim(resource_type_id) = '') ");
		List<WorkResourceFormMap> listUnSelectedWorkResourceFormMap = baseMapper.findByWhere(workResourceFormMap2);

		model.addAttribute("unSelectedWorkResourceList", listUnSelectedWorkResourceFormMap);
		model.addAttribute("selectedWorkResourceList", listSelectedWorkResourceFormMap);
	}
	
	/**
	 * 保存资源类型
	 * @param roleSelectGroups
	 * @return
	 */
	@ResponseBody
	@RequestMapping("saveWorkResourceType")
	@SystemLog(module="资源类型维护",methods="资源类型维护-保存资源类型")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String saveWorkResourceType() {
		WorkResourceTypeFormMap workResourceTypeFormMap = getFormMap(WorkResourceTypeFormMap.class);
		workResourceTypeFormMap.put("create_time", DateUtils.getStringDateTime());
		workResourceTypeFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		workResourceTypeFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
		try {
			workResourceTypeService.saveWorkResourceType(workResourceTypeFormMap);
		} catch (BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	
	/**
	 * 检索资源类型
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryWorkResourceType")
	public String queryWorkResourceType(Model model, HttpServletRequest request) {
		String siteId = ShiroSecurityHelper.getSiteId();
		String workResourceType = request.getParameter("work_resource_type");
		WorkResourceTypeFormMap workResourceTypeFormMap = new WorkResourceTypeFormMap();
		workResourceTypeFormMap.put("resource_type", workResourceType);
		workResourceTypeFormMap.put("site_id", ShiroSecurityHelper.getSiteId());

		List<WorkResourceTypeFormMap> listWorkResourceTypeFormMap = baseMapper.findByNames(workResourceTypeFormMap);
		if (!ListUtils.isNotNull(listWorkResourceTypeFormMap)) {
			String errMessage = "未检索到资源类型为：" + workResourceType.toUpperCase() + "的信息";
			log.error(errMessage);
			getWorkResourceBindData(model, siteId, "");
			model.addAttribute("errorMessage", errMessage);
			handlePageRes(model,request);
			return workResourceTypeMaintenanceUrl;
		}

		workResourceTypeFormMap = listWorkResourceTypeFormMap.get(0);
		String workResourceTypeId = workResourceTypeFormMap.getStr("id");
		getWorkResourceBindData(model, siteId, workResourceTypeId);

		model.addAttribute("workResourceTypeFormMap", workResourceTypeFormMap);
		model.addAttribute("successMessage", "检索成功");

		handlePageRes(model,request);
		return workResourceTypeMaintenanceUrl;
	}
	
	/**
	 * 删除资源类型
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("delWorkResourceType")
	public String delWorkResourceType(Model model, HttpServletRequest request) {
		String siteId = ShiroSecurityHelper.getSiteId();
		String workResourceTypeId = request.getParameter("work_resource_type_id");
		try {
			if (StringUtils.isEmpty(workResourceTypeId)) {
				throw new BusinessException("操作失败，参数不能为空！");
			}
			workResourceTypeService.delWorkResourceType(workResourceTypeId);
		} catch (BusinessException e) {
			String errMessage = e.getMessage();
			log.error(errMessage);
			getWorkResourceBindData(model, siteId, "");
			model.addAttribute("errorMessage", errMessage);
			handlePageRes(model,request);
			return workResourceTypeMaintenanceUrl;
		}
		getWorkResourceBindData(model, siteId, "");
		model.addAttribute("successMessage", "删除资源类型成功");
		handlePageRes(model,request);
		return workResourceTypeMaintenanceUrl;
	}
	
}