package com.iemes.mapper;

import java.util.List;
import java.util.Map;

import com.iemes.entity.ResFormMap;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.entity.FormMap;

public interface ResourcesMapper extends BaseMapper {
	public List<ResFormMap> findChildlists(ResFormMap map);

	public List<ResFormMap> findRes(ResFormMap map);

	public void updateSortOrder(List<ResFormMap> map);
	
	public List<ResFormMap> findUserResourcess(String userId);
	
	/**
	 * 返回所有权限，并且该角色拥有的权限追加isCheck字段，且值为1
	 * @param roleId
	 * @return
	 */
	List<Map<String,Object>> findResByRoleId(FormMap<String, String> map);
	
}
