package com.iemes.mapper.report;

import java.util.List;
import java.util.Map;

import com.iemes.mapper.base.BaseMapper;
import com.iemes.entity.FormMap;

public interface ReportMapper extends BaseMapper{

	/**
	 * 生产进度报表
	 * @param formMap
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> getScheduleReportData(FormMap<String, String> formMap)throws Exception;
	
	/**
	 * 生产记录报表（SFC列表）
	 * @param formMap
	 * @return
	 * @throws Exception
	 */
	List<Map<String, Object>> getProductionRecordListData(FormMap<String, String> formMap)throws Exception;
	
	/**
	 * 生产记录报表（工艺步骤列表）
	 * @param formMap
	 * @return
	 * @throws Exception
	 */
	List<Map<String, Object>> getProductionRecordFlowStepData(FormMap<String, String> formMap)throws Exception;
	
	/**
	 * 生产记录报表（生产步骤列表）
	 * @param formMap
	 * @return
	 * @throws Exception
	 */
	List<Map<String, Object>> getProductionRecordSfcStepData(FormMap<String, String> formMap)throws Exception;
	
	/**
	 * 生产记录报表（SFC装配信息列表）
	 * @param formMap
	 * @return
	 * @throws Exception
	 */
	List<Map<String, Object>> getProductionRecordAssembleListData(FormMap<String, String> formMap)throws Exception;
	
	/**
	 * 生产记录报表（SFC不良信息列表）
	 * @param formMap
	 * @return
	 * @throws Exception
	 */
	List<Map<String, Object>> getProductionRecordNcInfoData(FormMap<String, String> formMap)throws Exception;
}
