package com.iemes.mapper.workProcess;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.iemes.entity.SfcStepFormMap;
import com.iemes.entity.WorkShopInventoryFormMap;
import com.iemes.mapper.exception.DaoException;
import com.iemes.mapper.base.BaseMapper;

public interface WorkPodPanelMapper extends BaseMapper {

	/**
	 * 获取当前操作、资源上生产的SFC列表
	 * @param operationId
	 * @param workResourceId
	 */
	List<Map<String, String>> getSfcOnOperation(@Param("operationId")String operationId);
	
	/**
	 * 获取当前操作、资源上排队的SFC列表
	 * @param operationId
	 * @param workResourceId
	 */
	List<SfcStepFormMap> getSfcWaitOnOperation(@Param("operationId")String operationId, @Param("workResourceId")String workResourceId, 
			@Param("sfc")String sfc);
	
	/**
	 * 根据面板ID获取按钮组
	 * @param podPanelId
	 * @return
	 */
	List<Map<String, String>> getButtonsByPodPanel(@Param("podPanelId")String podPanelId);
	
	/**
	 * 获取装配列表
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	List<Map<String, Object>> getAssembleList(Map<String, String> map)throws DaoException;
	
	/**
	 * 查找下一个步骤（不包含维修操作）
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	List<Map<String, String>> getNextOperation(Map<String, String> map)throws DaoException;
	
	/**
	 * 查找下一个步骤（只查维修操作）
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	List<Map<String, String>> getNextRepairOperation(Map<String, String> map)throws DaoException;
	
	/**
	 * 获取有效的装配物料批次
	 * @param param
	 * @return
	 */
	List<WorkShopInventoryFormMap> getValidItemBatch(Map<String, String> param);
	
	/**
	 * 获取某个物料批次剩余数量
	 * @param param
	 * @return
	 */
	int getValidItemBatchNum(Map<String, String> param);
}
