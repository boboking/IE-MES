package com.iemes.exception;

/**
 * 业务层异常
 * @author huahao
 *
 */
public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 6369672749671888974L;
	
	public BusinessException(String str) {
        super(str);
    }

}
